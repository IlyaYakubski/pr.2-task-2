import java.util.Scanner;

public class p2t2 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        double x = scanner.nextDouble();
        System.out.println("x = " + x);
        double y = scanner.nextDouble();
        System.out.println("y = " + y);

        System.out.println("x + y = " + (x + y));
        System.out.println("x - y = " + (x - y));
        System.out.println("x * y = " + (x * y));
        System.out.println("x / y = " + (x / y));
    }
}